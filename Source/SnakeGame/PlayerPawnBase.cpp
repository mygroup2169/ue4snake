// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeActorBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	SetActorTickInterval(SnakeActor->getCurrentTickInterval());
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	IsMoved = false;
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horisontal", this, &APlayerPawnBase::HandlePlayerHorisontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeActorBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor) && SnakeActor->LastMovingDirection != EMovementDirection::DOWN && !IsMoved) {
		if (value > 0) {
			SnakeActor->LastMovingDirection = EMovementDirection::UP;
			IsMoved = true;
		}
		else if (value < 0 && SnakeActor->LastMovingDirection != EMovementDirection::UP && !IsMoved) {
			SnakeActor->LastMovingDirection = EMovementDirection::DOWN;
			IsMoved = true;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorisontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastMovingDirection != EMovementDirection::LEFT && !IsMoved) {
			SnakeActor->LastMovingDirection = EMovementDirection::RIGHT;
			IsMoved = true;
		}
		else if (value < 0 && SnakeActor->LastMovingDirection != EMovementDirection::RIGHT && !IsMoved) {
			SnakeActor->LastMovingDirection = EMovementDirection::LEFT;
			IsMoved = true;
		}
	}
}

