// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActorBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

class ASnakeElementBase;

// Sets default values
ASnakeActorBase::ASnakeActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMovingDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeActorBase::BeginPlay()
{
	Super::BeginPlay();
	setCurrentTickInterval(MovementSpeed);
	SetActorTickInterval(currentTickInterval);
	AddSnakeElement(1);
}

// Called every frame
void ASnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	if (SpeedMultiplier <=3 && SpeedMultiplier >= 0.5) {
		setCurrentTickInterval(MovementSpeed / SpeedMultiplier);
		SetActorTickInterval(currentTickInterval);
	}
}

void ASnakeActorBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation;
		if (SnakeElements.Num()!=0) {
			FVector AddPosition(0, 0, ElementSize);
			NewLocation = FVector(SnakeElements.Last()->GetActorLocation()+ AddPosition);
		}
		else {
			NewLocation = FVector(SnakeElements.Num()*ElementSize, 0, 0);
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeActorBase::AddSpeedMultiplier(float NewSpeedMultiplier)
{
	SpeedMultiplier += NewSpeedMultiplier;
}

void ASnakeActorBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMovingDirection) {
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeActorBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor *Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

float ASnakeActorBase::getCurrentTickInterval()
{
	return currentTickInterval;
}

void ASnakeActorBase::setCurrentTickInterval(float newCurrentTickInterval)
{
	currentTickInterval = newCurrentTickInterval;
}

