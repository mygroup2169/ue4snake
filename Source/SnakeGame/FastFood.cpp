// Fill out your copyright notice in the Description page of Project Settings.


#include "FastFood.h"
#include "SnakeActorBase.h"

// Sets default values
AFastFood::AFastFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFastFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFastFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFastFood::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast <ASnakeActorBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			Snake->AddSpeedMultiplier(0.2f);
			Destroy();
		}
	}
}

