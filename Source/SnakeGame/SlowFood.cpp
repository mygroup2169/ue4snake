// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowFood.h"
#include "SnakeActorBase.h"

// Sets default values
ASlowFood::ASlowFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowFood::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast <ASnakeActorBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			Snake->AddSpeedMultiplier(-0.2f);
			Destroy();
		}
	}
}